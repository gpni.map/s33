// console.log("acticvity")


console.log(fetch("https://jsonplaceholder.typicode.com/todos"))

fetch("https://jsonplaceholder.typicode.com/todos")

async function getList() {

	let result = await fetch("https://jsonplaceholder.typicode.com/todos")
	console.log(result)
	console.log(result.body)
	let json = await result.json()
	console.log(json)
}
getList();

let titles = []

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(data => {
   data.map((element) => {
   	titles.push(element.title)
  })
})

console.log(titles)


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos", {
	method:"POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		title: "New List",
		completed: false
	})

})
.then((response) => response.json())
.then((json) => console.log(json))



fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Mirror the data structure",
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "To Do Update",
		description: "Updating of to do item",
		status: "completed",
		dateCompleted: "2022-08-26",
		userId: 250
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "mirror again the list"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
.then((response) => response.json())
.then((json) => console.log(json))